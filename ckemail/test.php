<?php

require_once('class.smtp.php');
require_once('class.pop3.php');
require_once('class.phpmailer.php');

$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

$mail->IsSMTP(); // telling the class to use SMTP

try {
  $mail->Host       = "ssl://smtp.gmail.com"; // SMTP server
  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
  $mail->SMTPAuth   = true;                  // enable SMTP authentication
  $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
  $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
  $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
  $mail->Username   = "webwingt@gmail.com";  // GMAIL username
  $mail->Password   = "webwing30";            // GMAIL password

  $mail->AddReplyTo('webwing.testing@gmail.com', 'Webwing');

  $mail->AddAddress('gayatrid@webwingtechnologies.com', 'Gayatri Daund');
  
  $mail->SetFrom('webwing.testing@gmail.com', 'Webwing');
  
  $mail->Subject = 'PHPMailer Test Mail';
  
  $mail->MsgHTML("This is the test mail for local");
  
  $mail->Send();
  
  echo "Message Sent OK</p>\n";
} 
catch (phpmailerException $e) 
{
  echo $e->errorMessage(); //Pretty error messages from PHPMailer
} 
catch (Exception $e) 
{
  echo $e->getMessage(); //Boring error messages from anything else!
}

?>