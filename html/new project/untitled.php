<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<link rel="stylesheet" href="<?php echo $this->getSkinUrl('css/accordion.css') ?>" />
<link rel="stylesheet" href="<?php echo $this->getSkinUrl('css/responsivetabs.css') ?>" />


<div id="messages_product_view"><?php echo $this->getMessagesBlock()->toHtml() ?></div>
<div class="page-title">
    <h1><?php echo Mage::helper('contacts')->__('Contact Us | FAQ') ?></h1>
</div>
<!-- FAQ start -->
<!--about us page start here-->   
<div class="about-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-24 col-md-24 col-lg-24">

                <div class="search-form">
                   <div class="row">
                    <div class="col-lg-21">
                        <input class="input-text required-entry validate-email" placeholder="Search For Answer"/>
                    </div>
                    <div class="col-lg-2">
                        <div class="account-login"> <button class="button btn-bg-bor" title="Submit" type="submit"><span>Search</span></button></div>
                    </div>
                </div>
                </div>
                 <div class="my-acct-section">
                 <div data-responsive-tabs>
            <nav>
                <ul>
                <li><a href="#one">Topic</a></li>
                <li><a href="#two">Topic1</a></li>
                <li><a href="#three">Topic2</a></li>
                <li><a href="#four">Topic3</a></li>
                <li><a href="#five">Topic4</a></li>
                </ul>
            </nav>
            <div class="content">
                <section id="one">
                     <div id='contct'>
                   <ul>
                       <li class='has-sub active'>
                           <a href='#'><span>What Is Notice Website?</span></a>
                           
                           <ul class="faq-hide" style="display:block;">
                               <li>                                
                                   <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>                                  
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>Should I expect shade variations or imperfection in this product?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                    <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>How To post Notice Ads?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                      
                       
                       
                       
                   </ul>
               </div>
                </section>
                
                
                <section id="two">
                 <div id='contct'>
                   <ul>
                       <li class='has-sub active'>
                           <a href='#'><span>What Is Notice Website?</span></a>
                           
                           <ul class="faq-hide" style="display:block;">
                               <li>                                
                                   <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>                                  
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>Should I expect shade variations or imperfection in this product?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                    <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>How To post Notice Ads?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                        <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                      
                       
                       
                       
                   </ul>
               </div>               
                </section>
                <section id="three">
                 <div id='contct'>
                   <ul>
                       <li class='has-sub active'>
                           <a href='#'><span>What Is Notice Website?</span></a>
                           
                           <ul class="faq-hide" style="display:block;">
                               <li>                                
                                   <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>                                  
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>Should I expect shade variations or imperfection in this product?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                    <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>How To post Notice Ads?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                      
                       
                       
                      
                       
                       
                       
                   </ul>
               </div>               
                </section>
                                        
                <section id="four">
                 <div id='contct'>
                   <ul>
                       <li class='has-sub active'>
                           <a href='#'><span>What Is Notice Website?</span></a>
                           
                           <ul class="faq-hide" style="display:block;">
                               <li>                                
                                   <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>                                  
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>Should I expect shade variations or imperfection in this product?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                    <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>How To post Notice Ads?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                        <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                      
                       
                       
                       
                   </ul>
               </div>                   
                </section>
            <section id="five">
                 <div id='contct'>
                   <ul>
                       <li class='has-sub active'>
                           <a href='#'><span>What Is Notice Website?</span></a>
                           
                           <ul class="faq-hide" style="display:block;">
                               <li>                                
                                   <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>                                  
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>Should I expect shade variations or imperfection in this product?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                    <div class="faq-text">
                                       
                                       <span class="inner-ansone">Secoin encaustic cement tiles are hand-made, aesthetic tiles with many colored patterns used for floor and wall coverings. Our tiles are made 
with two layers: The first layer (about 2-3mm thick) is a mixture of white cement, pigment, stone powder and additives which is hand-poured into 
divider mould to create desired patterned. The second layer is made of grey cement and sand to ensure the strength of tiles. 
                                       </span>
                                       <div class="clr"></div>
                                     
                                        
                                   </div>
                               </li>
                           </ul>
                       </li>
                       <li class='has-sub'>
                           <a href='#'><span>How To post Notice Ads?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       <li class='has-sub'>
                           <a href='#'><span>At vero eos et accusamus et iusto odio dignissimos?</span></a>
                           <ul class="faq-hide">
                               <li>                                
                                   <div class="faq-text">
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span><br/><br/><br/>
                                       <span class="inner-que">How do I know my freelancer is going to get the job done?</span>
                                       <span class="inner-ans">It’s easy. First think about the skills you need, then describe your job and post it. Next wait for freelancers to submit their best proposals for your review. You can also invite freelancers to submit proposals from potential matches we highlight, or search our entire freelance community for that perfect fit. 
                                       </span>
                                   </div>
                               </li>
                           </ul>
                       </li>
                       
                       
                      
                       
                       
                      
                       
                       
                       
                   </ul>
               </div>               
                </section>                            
                                            
            </div>
        </div>
</div>
            </div>
            
        </div>
    </div>
</div>   

<!--about us page end here-->   

<!-- FAQ end -->
<?php //echo $this->getChildHtml('em_area_contacts_map');?>

<div class="row">
    <div class="col-sm-12">
        <form action="<?php echo $this->getFormAction(); ?>" id="contactForm" method="post">
            <div class="fieldset">
                <h2 class="legend"><?php echo Mage::helper('contacts')->__('Contact Information') ?></h2>
                <ul class="form-list">
                    <li class="fields">
                        <div class="field">
                            <label for="name" class="required"><em>*</em><?php echo Mage::helper('contacts')->__('Name') ?></label>
                            <div class="input-box">
                        <input name="name" id="name" title="<?php echo Mage::helper('core')->quoteEscape(Mage::helper('contacts')->__('Name')) ?>" value="<?php echo $this->escapeHtml($this->helper('contacts')->getUserName()) ?>" class="input-text required-entry" type="text" />
                            </div>
                        </div>
                        <div class="field">
                            <label for="email" class="required"><em>*</em><?php echo Mage::helper('contacts')->__('Email') ?></label>
                            <div class="input-box">
                        <input name="email" id="email" title="<?php echo Mage::helper('core')->quoteEscape(Mage::helper('contacts')->__('Email')) ?>" value="<?php echo $this->escapeHtml($this->helper('contacts')->getUserEmail()) ?>" class="input-text required-entry validate-email" type="text" />
                            </div>
                        </div>
                    </li>
                    <!-- <li>
                        <label for="telephone"><?php echo Mage::helper('contacts')->__('Telephone') ?></label>
                        <div class="input-box">
                            <input name="telephone" id="telephone" title="<?php echo Mage::helper('contacts')->__('Telephone') ?>" value="" class="input-text" type="text" />
                        </div>
                    </li> -->
                    <li class="fields">
                        <div class="field">
                            <label for="subject" class="required"><em>*</em><?php echo Mage::helper('contacts')->__('Subject') ?></label>
                            <div class="input-box">
                        <input name="subject" id="subject" title="<?php //echo Mage::helper('core')->quoteEscape(Mage::helper('contacts')->__('Subject')) ?>" value="<?php //echo $this->escapeHtml($this->helper('contacts')->getUserName()) ?>" class="input-text required-entry" type="text" />
                            </div>
                        </div>
                        <div class="field">
                            <label for="order" class="required"><em>*</em><?php echo Mage::helper('contacts')->__('Order') ?></label>
                            <div class="input-box">
                        <input name="order" id="order" title="<?php //echo Mage::helper('core')->quoteEscape(Mage::helper('contacts')->__('Email')) ?>" value="<?php //echo $this->escapeHtml($this->helper('contacts')->getUserEmail()) ?>" class="input-text required-entry" type="text" />
                            </div>
                        </div>
                    </li>
                    <li class="wide">
                        <label for="comment" class="required"><em>*</em><?php echo Mage::helper('contacts')->__('Message') ?></label>
                        <div class="input-box">
                            <textarea name="comment" id="comment" title="<?php echo Mage::helper('contacts')->__('Message') ?>" class="required-entry input-text" cols="5" rows="3"></textarea>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="buttons-set">
                <input type="text" name="hideit" id="hideit" value="" style="display:none !important;" />
                <button type="submit" title="<?php echo Mage::helper('core')->quoteEscape(Mage::helper('contacts')->__('Submit')) ?>" class="button"><span><span><?php echo Mage::helper('contacts')->__('Submit') ?></span></span></button>
                <p class="required"><?php echo Mage::helper('contacts')->__('* Required Fields') ?></p>
            </div>
        </form>
        <script type="text/javascript">
        //<![CDATA[
            var contactForm = new VarienForm('contactForm', true);
        //]]>
        </script>
    </div>
    <div class="col-sm-12">
        <?php echo $this->getChildHtml('em_area_contacts_block');?>
    </div>
</div>

