<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Helper_Data
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED                  = 
        'perficient_manufacturer/general/is_enabled';
    const XML_PATH_DEFAULT_ATTRIBUTE_CODE   = 
        'perficient_manufacturer/frontend/manufacturers_attribute_code';
    const XML_PATH_DEFAULT_META_TITLE       = 
        'perficient_manufacturer/frontend/meta_title';
    const XML_PATH_DEFAULT_META_KEYWORDS    = 
        'perficient_manufacturer/frontend/meta_keywords';
    const XML_PATH_DEFAULT_META_DESCRIPTION = 
        'perficient_manufacturer/frontend/meta_description';

    
    /**
     * Set Is Module Enabled
     * 
     * @param string $value value of data
     *
     * @return void
     */
    public function setIsModuleEnabled($value)
    {
        Mage::getModel('core/config')->saveConfig(
            self::XML_PATH_ENABLED, $value
        );
    }//end setIsModuleEnabled()


    /**
     * Retrieve default title for manufacturers
     *
     * @return string
     */
    public function getManufacturersAttributeCode()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEFAULT_ATTRIBUTE_CODE);
    }//end getManufacturersAttributeCode()


    /**
     * Retrieve default title for manufacturers
     *
     * @return string
     */
    public function getDefaultTitle()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEFAULT_META_TITLE);
    }//end getDefaultTitle()


    /**
     * Retrieve default meta keywords for manufacturers
     *
     * @return string
     */
    public function getDefaultMetaKeywords()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEFAULT_META_KEYWORDS);
    }//end getDefaultMetaKeywords()


    /**
     * Retrieve default meta description for manufacturers
     *
     * @return string
     */
    public function getDefaultMetaDescription()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEFAULT_META_DESCRIPTION);
    }//end getDefaultMetaDescription()


    /**
     * Retrieve Template processor for Block Content
     *
     * @return Varien_Filter_Template
     */
    public function getBlockTemplateProcessor()
    {
        return Mage::getModel('perficient_manufacturer/template_filter');
    }//end getBlockTemplateProcessor()


}//end class
