<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Meta
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Meta 
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{


    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('perficient_manufacturer')->__('Meta Information');
    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }//end getTabTitle()


    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }//end canShowTab()


    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }//end isHidden()


    /**
     * Set form id prefix, set values if manufacturer is editing
     *
     * @return 
     * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Meta
     */
    protected function _prepareForm()
    {
        $form         = new Varien_Data_Form();
        $htmlIdPrefix = 'manufacturer_meta_';
        $form->setHtmlIdPrefix($htmlIdPrefix);

        $fieldsetHtmlClass = 'fieldset-wide';

        $model = Mage::registry('current_manufacturer');

        Mage::dispatchEvent(
            'adminhtml_manufacturer_edit_tab_meta_before_prepare_form',
            array(
             'model' => $model, 
             'form'  => $form,
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Meta Information'
        );
        // add meta information fieldset
        $fieldset = $form->addFieldset(
            'default_fieldset', 
            array(
             'legend' => $label,
             'class'  => $fieldsetHtmlClass,
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__('Meta Keywords');

        $fieldset->addField(
            'meta_keywords', 
            'textarea', 
            array(
             'name'     => 'meta_keywords',
             'label'    => $label,
             'disabled' => (bool)$model->getIsReadonly(),
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Meta Description'
        );

        $fieldset->addField(
            'meta_description', 'textarea', 
            array(
             'name'     => 'meta_description',
             'label'    => $label,
             'disabled' => (bool)$model->getIsReadonly(),
            )
        );

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }//end _prepareForm()


}//end class
