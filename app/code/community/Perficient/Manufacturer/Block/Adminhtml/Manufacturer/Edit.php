<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit 
    extends Mage_Adminhtml_Block_Widget_Form_Container
{


    /**
     * Initialize manufacturer edit page. Set management buttons
     *
     */
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_controller = 'adminhtml_manufacturer';
        $this->_blockGroup = 'perficient_manufacturer';

        parent::__construct();

        $this->_updateButton(
            'save', 'label', 
            Mage::helper('perficient_manufacturer')->__('Save Manufacturer')
        );
        $this->_updateButton(
            'delete', 'label', 
            Mage::helper('perficient_manufacturer')->__('Delete Manufacturer')
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Save and Continue Edit'
        );

        $this->_addButton(
            'save_and_edit_button', 
            array(
             'label'   => $label,
             'onclick' => 'saveAndContinueEdit()',
             'class'   => 'save',
            ), 100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
            editForm.submit($('edit_form').action + 'back/edit/');}";
    }//end __construct()


    /**
     * Get current loaded manufacturer ID
     *
     * @return integer
     */
    public function getManufacturerId()
    {
        return Mage::registry('current_manufacturer')->getId();
    }//end getManufacturerId()


    /**
     * Get header text for manufacturer edit page
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_manufacturer')->getId()) {
            return $this->htmlEscape(
                Mage::registry('current_manufacturer')->getTitle()
            );
        } else {
            return Mage::helper('perficient_manufacturer')->__(
                'New Manufacturer'
            );
        }
    }//end getHeaderText()


    /**
     * Get form action URL
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('*/*/save');
    }//end getFormActionUrl()


}//end class
