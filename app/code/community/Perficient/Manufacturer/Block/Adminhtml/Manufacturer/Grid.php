<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Grid
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Grid 
    extends Mage_Adminhtml_Block_Widget_Grid
{


    /**
     * Set defaults
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('manufacturerGrid');
        $this->setDefaultSort('manufacturer_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }//end __construct()


    /**
     * Instantiate and prepare collection
     *
     * @return Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel(
            'perficient_manufacturer/manufacturer_collection'
        );

        $tableEavOptVal  = Mage::getModel('core/resource')
            ->getTableName('eav_attribute_option_value');
        $tableNameEavOpt = Mage::getModel('core/resource')
            ->getTableName('eav_attribute_option');

        $collection->getSelect()->distinct()->join(
            array('eaov' => $tableEavOptVal), 
            'main_table.manufacturer = eaov.option_id', 
            array('manufacturer_name' => 'value')
        )->join(
            array('eao' => $tableNameEavOpt), 
            'eao.option_id = eaov.option_id', 
            array()
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $collection->addStoresVisibility();
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }//end _prepareCollection()


    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'manufacturer_id', 
            array(
             'header' => Mage::helper('perficient_manufacturer')->__('ID'),
             'type'   => 'number',
             'width'  => '1',
             'index'  => 'manufacturer_id',
            )
        );
        
        $header = Mage::helper('perficient_manufacturer')->__(
            'Manufacturer Title'
        );

        $this->addColumn(
            'manufacturer_name', 
            array(
             'header' => $header,
             'type'   => 'text',
             'index'  => 'manufacturer_name',
            )
        );

        $header = Mage::helper('perficient_manufacturer')->__('Update Time');
        
        $this->addColumn(
            'update_time', 
            array(
             'header' => $header,
             'type'   => 'datetime',
             'index'  => 'update_time',
            )
        );
        $options = Mage::getModel(
            'perficient_manufacturer/status'
        )->getAllOptions();
        $this->addColumn(
            'status', 
            array(
             'header'  => Mage::helper('perficient_manufacturer')->__('Status'),
             'align'   => 'center',
             'width'   => 1,
             'index'   => 'status',
             'type'    => 'options',
             'options' => $options,
            )
        );

        $header = Mage::helper('perficient_manufacturer')->__('Visible In');
        
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'visible_in', 
                array(
                 'header'     => $header,
                 'type'       => 'store',
                 'index'      => 'stores',
                 'sortable'   => false,
                 'store_view' => true,
                 'width'      => 200,
                )
            );
        }

        $header  = Mage::helper('perficient_manufacturer')->__('Action');
        $caption = Mage::helper('perficient_manufacturer')->__('Edit');
        $this->addColumn(
            'action', 
            array(
             'header'    => $header,
             'width'     => '50',
             'type'      => 'action',
             'align'     => 'center',
             'getter'    => 'getId',
             'actions'   => array(
                             array(
                              'caption' => $caption,
                              'url'     => array('base' => '*/*/edit'),
                              'field'   => 'id',
                             ),
                            ),
             'filter'    => false,
             'sortable'  => false,
             'index'     => 'stores',
             'is_system' => true,
            )
        );
        return parent::_prepareColumns();
    }//end _prepareColumns()


    /**
     * Prepare Mass Action
     * 
     * @return Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Grid
     */
    protected function _prepareMassaction()
    {
        $confirm = Mage::helper('perficient_manufacturer')->__(
            'Are you sure you want to delete these manufacturer?'
        );
        $this->setMassactionIdField('manufacturer_id');
        $this->getMassactionBlock()->setFormFieldName('manufacturer');
        $this->getMassactionBlock()->addItem(
            'delete', 
            array(
             'label'   => Mage::helper('perficient_manufacturer')->__('Delete'),
             'url'     => $this->getUrl('*/*/massDelete'),
             'confirm' => $confirm,
            )
        );
        return $this;
    }//end _prepareMassaction()


    /**
     * Grid row URL getter
     *
     * @param object $row row data object
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }//end getRowUrl()


    /**
     * Define row click callback
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            '*/*/grid', 
            array('_current' => true)
        );
    }//end getGridUrl()


    /**
     * Add store filter
     *
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column column data
     * 
     * @return Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Grid
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getIndex() == 'stores') {
            $this->getCollection()->addStoreFilter(
                $column->getFilter()->getCondition(), false
            );
        } elseif ($column->getIndex() == 'manufacturer_name') {
            $this->getCollection()->addManufacturerNameFilter(
                $column->getFilter()->getCondition()
            );
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }//end _addColumnFilterToCollection()


}//end class