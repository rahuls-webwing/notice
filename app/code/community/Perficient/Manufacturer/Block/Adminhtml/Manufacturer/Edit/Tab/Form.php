<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Form
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Form 
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{


    /**
     * Load Wysiwyg on demand and Prepare layout
     * 
     * @return void
     */
    protected function _prepareLayout()
    { 
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }//end _prepareLayout()


    /**
     * Set form id prefix, set values if manufacturer is editing
     *
     * @return 
     * Perficient_Manufacturer_Block_Adminhtml_Manufacturer_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form         = new Varien_Data_Form();
        $htmlIdPrefix = 'manufacturer_information_';
        $form->setHtmlIdPrefix($htmlIdPrefix);
        $fieldsetHtmlClass = 'fieldset-wide';
        $wysiwygConfig     = Mage::getSingleton(
            'cms/wysiwyg_config'
        )->getConfig(array('tab_id' => $this->getTabId()));

        /* @var $model Perficient_Manufacturer_Model_Manufacturer */
        $model    = Mage::registry('current_manufacturer');
        $contents = $model->getDescription();
        $heading  = Mage::helper('perficient_manufacturer')->__(
            'Manufacturer information'
        );

        $fieldset = $form->addFieldset(
            'base_fieldset', 
            array(
             'legend' => $heading,
             'class'  => $fieldsetHtmlClass,
            )
        );

        if ($model->getManufacturerId()) {
            $fieldset->addField(
                'manufacturer_id', 
                'hidden', 
                array('name' => 'manufacturer_id')
            );
        }

        $label   = Mage::helper('perficient_manufacturer')->__('Manufacturer');
        $options = Mage::getModel('perficient_manufacturer/options')
            ->getManufacturers();

        $fieldset->addField(
            'manufacturer', 
            'select', 
            array(
             'label'    => $label,
             'name'     => 'manufacturer',
             'required' => true,
             'options'  => $options,
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__('Identifier');

        $fieldset->addField(
            'identifier', 
            'text', 
            array(
             'label'    => $label,
             'name'     => 'identifier',
             'required' => false,
            )
        );

        $label   = Mage::helper('perficient_manufacturer')->__('Status');
        $options = Mage::getModel('perficient_manufacturer/status')
            ->getAllOptions();
        $fieldset->addField(
            'status', 
            'select', 
            array(
             'label'    => $label,
             'name'     => 'status',
             'required' => 'true',
             'disabled' => (bool)$model->getIsReadonly(),
             'options'  => $options,
            )
        );

        if (!$model->getId()) {
            $model->setData(
                'status', Perficient_Manufacturer_Model_Status::STATUS_ENABLED
            );
        }
        
        $label   = Mage::helper('perficient_manufacturer')->__('Visible In');
        $options = Mage::getSingleton('adminhtml/system_store')
            ->getStoreValuesForForm(false, true);
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_ids', 
                'multiselect', 
                array(
                 'name'     => 'store_ids[]',
                 'label'    => $label,
                 'required' => true,
                 'values'   => $options,
                 'value'    => $model->getStoreIds(),
                )
            );
        } else {
            $fieldset->addField(
                'store_id', 
                'hidden', 
                array(
                 'name'  => 'store_ids[]',
                 'value' => Mage::app()->getStore(true)->getId(),
                )
            );
            $model->setStoreIds(Mage::app()->getStore(true)->getId());
        }

        $label = Mage::helper('perficient_manufacturer')
            ->__('Display on Frontend');

        $options = Mage::getModel(
            'perficient_manufacturer/status'
        )->getAllOptions();

        $fieldset->addField(
            'is_display_home', 
            'select', 
            array(
             'label'    => $label,
             'name'     => 'is_display_home',
             'required' => 'true',
             'disabled' => (bool)$model->getIsReadonly(),
             'options'  => $options,
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Manufacturer Logo'
        );
        
        $fieldset->addField(
            'manufacturer_logo', 
            'Thumbnail', 
            array(
             'label'    => $label,
             'required' => false,
             'name'     => 'manufacturer_logo',
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Manufacturer Banner'
        );

        $fieldset->addField(
            'manufacturer_banner', 
            'Thumbnail', 
            array(
             'label'    => $label,
             'required' => false,
             'name'     => 'manufacturer_banner',
            )
        );

        $label = Mage::helper('perficient_manufacturer')->__('Description');

        $fieldset->addField(
            'description', 
            'editor', 
            array(
             'name'     => 'description',
             'label'    => $label,
             'title'    => $label,
             'style'    => 'height:36em',
             'required' => false,
             'config'   => $wysiwygConfig,
            )
        );    

        $label = Mage::helper('perficient_manufacturer')->__('Sort Order');

        $fieldset->addField(
            'sort_order', 
            'text', 
            array(
             'label' => $label,
             'name'  => 'sort_order',
            )
        );
        $form->setValues($model->getData());
        $this->setForm($form);
        return $this;
    }//end _prepareForm()


    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('perficient_manufacturer')
            ->__('Manufacturer Information');
    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }//end getTabTitle()


    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }//end canShowTab()


    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }//end isHidden()


}//end class
