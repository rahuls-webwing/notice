<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_Adminhtml_Manufacturer
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_Adminhtml_Manufacturer 
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{


    /**
    * Initialize manufacturer manage page
    *
    * @return void
    */
    public function __construct()
    {
        $header = Mage::helper('perficient_manufacturer')->__(
            'Manage Manufacturer'
        );

        $label = Mage::helper('perficient_manufacturer')->__(
            'Add Manufacturer'
        );
        
        $this->_controller     = 'adminhtml_manufacturer';
        $this->_blockGroup     = 'perficient_manufacturer';
        $this->_headerText     = $header;
        $this->_addButtonLabel = $label;
        parent::__construct();
    }//end __construct()


}//end class
