<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_View
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_View 
    extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Manufacturer Property.
     * 
     * @var object
     */
    protected $_manufacturer;

    /**
     * Default Toolbar Block Property.
     * 
     * @var object
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';
    
    /**
     * Manufacturer Collection Property.
     * 
     * @var object
     */
    protected $_manufacturerCollection;


    /**
     * Retrieve Manufacturer instance
     *
     * @return Perficient_Manufacturer_Model_Manufacturer
     */
    public function getManufacturer()
    {
        $manufacturerId = $this->getRequest()->getParam(
            'manufacturer_id', false
        );
        if (is_null($this->_manufacturer)) {
            if ($manufacturerId) {
                $this->_manufacturer = Mage::getModel(
                    'perficient_manufacturer/manufacturer'
                )->setStoreId(Mage::app()->getStore()->getId())
                 ->load($manufacturerId);
            } else {
                $this->_manufacturer = Mage::getSingleton(
                    'perficient_manufacturer/manufacturer'
                );
            }
        }
        return $this->_manufacturer; 
    }//end getManufacturer()


     /**
     * Retrieve Manufacturer collection
     *
     * @return Perficient_Manufacturer_Model_Resource_Manufacturer_Collection
     */
    protected function _getManufacturerCollection() 
    {
        $manufacturerCode = Mage::helper(
            'perficient_manufacturer'
        )->getManufacturersAttributeCode();
        if (is_null($this->_manufacturerCollection)) {
            $manufacturerId = $this->getManufacturer()->getManufacturer();
        
            $this->_manufacturerCollection = 
                Mage::getResourceModel('catalog/product_collection');
            $this->_manufacturerCollection->setVisibility(
                Mage::getSingleton(
                    'catalog/product_visibility'
                )->getVisibleInCatalogIds()
            );
            
            $this->_manufacturerCollection = 
                $this->_addProductAttributesAndPrices(
                    $this->_manufacturerCollection
                )
            ->addStoreFilter()
            ->addAttributeToFilter($manufacturerCode, $manufacturerId);
        
        }
        
        return $this->_manufacturerCollection;
    }//end _getManufacturerCollection()


    /**
     * Retrieve loaded Manufacturer collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getManufacturerCollection()
    {
        return $this->_getManufacturerCollection();
    }//end getManufacturerCollection()


    /**
     * Prepare global layout
     *
     * @return Perficient_Manufacturer_Block_View
     */
    protected function _prepareLayout()
    {
        $manufacturer = $this->getManufacturer();
        $helper       = Mage::helper('perficient_manufacturer');
        // show breadcrumbs
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbs->addCrumb(
                    'home', 
                    array(
                     'label' => $helper->__('Home'), 
                     'title' => $helper->__('Go to Home Page'), 
                     'link'  => Mage::getBaseUrl(),
                    )
                );
                $breadcrumbs->addCrumb(
                    'manufacturers_list', 
                    array(
                     'label' => $helper->__('Manufacturers'), 
                     'title' => $helper->__('Manufacturers'), 
                     'link'  => Mage::getUrl('manufacturers'),
                    )
                );
                $label = Mage::getModel('perficient_manufacturer/manufacturer')
                    ->getManufacturerName(
                        $manufacturer->getManufacturer(), 
                        Mage::app()->getStore()->getId()
                    );
                $breadcrumbs->addCrumb(
                    'manufacturers_view', 
                    array(
                     'label' => $label, 
                     'title' => $manufacturer->getIdentifier(),
                    )
                );
        }

        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle($manufacturer->getIdentifier());
            $head->setKeywords(
                $manufacturer->getMetaKeywords() 
                ? $manufacturer->getMetaKeywords() : 
                $helper->getDefaultMetaKeywords()
            );
            $head->setDescription(
                $manufacturer->getMetaDescription() 
                ? $manufacturer->getMetaDescription() : 
                $helper->getDefaultMetaDescription()
            );
        }

        return parent::_prepareLayout();
    }//end _prepareLayout()

    
    /**
     * Get Mode Method
     * 
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }//end getMode()


    /**
     * Before HTML
     *
     * @return Mage_Catalog_Block_Product_Abstract
     */
    protected function _beforeToHtml()
    {  
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getManufacturerCollection();
    
        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        } 

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent(
            'catalog_block_product_list_collection', 
            array(
             'collection' => $this->_getManufacturerCollection(),
            )
        );

        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }//end _beforeToHtml()


    /**
     * Get Toolbar Block
     * 
     * @return object
     */
    public function getToolbarBlock()
    {   
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock(
            $this->_defaultToolbarBlock, microtime()
        );
        return $block;
    }//end getToolbarBlock()


    /**
     * Get Toolbar Block Method
     * 
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }//end getToolbarHtml()


    /**
     * Get Toolbar Block Method
     *
     * @param object $collection collection object
     * 
     * @return Perficient_Manufacturer_Block_View
     */
    public function setCollection($collection)
    {
        $this->_manufacturerCollection = $collection;
        return $this;
    }//end setCollection()


}//end class
