<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Block_List
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Block_List extends Mage_Core_Block_Template
{
    
    /**
     * Manufacturers Collection Property.
     * 
     * @var object
     */
    protected $_manufacturersCollection;


    /**
     * Retrieve Manufacturers collection
     *
     * @return Perficient_Manufacturer_Model_Resource_Manufacturer_Collection
     */
    protected function _getManufacturersCollection()
    {
        if (is_null($this->_manufacturersCollection)) {
            $this->_manufacturersCollection = 
                Mage::getResourceModel(
                    'perficient_manufacturer/manufacturer_collection'
                )->distinct(true)
                 ->addStoreFilter(Mage::app()->getStore()->getId())
                 ->addFieldToFilter(
                     'status',
                     Perficient_Manufacturer_Model_Status::STATUS_ENABLED
                 )->addOrder('sort_order', 'asc');
        }
        return $this->_manufacturersCollection;
    }//end _getManufacturersCollection()


    /**
     * Retrieve loaded Manufacturers collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getManufacturersCollection()
    {
        return $this->_getManufacturersCollection();
    }//end getManufacturersCollection()


    /**
     * Prepare global layout
     *
     * @return Perficient_Manufacturer_Block_List
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('perficient_manufacturer');
        // show breadcrumbs
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb(
                'home', 
                array(
                 'label' => $helper->__('Home'), 
                 'title' => $helper->__('Go to Home Page'), 
                 'link'  => Mage::getBaseUrl(),
                )
            );
            $breadcrumbs->addCrumb(
                'manufacturers_list', 
                array(
                 'label' => $helper->__('Manufacturers'), 
                 'title' => $helper->__('Manufacturers'),
                )
            );
        }
        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle($helper->getDefaultTitle());
            $head->setKeywords($helper->getDefaultMetaKeywords());
            $head->setDescription($helper->getDefaultMetaDescription());
        }

        return parent::_prepareLayout();
    }//end _prepareLayout()


}//end class
