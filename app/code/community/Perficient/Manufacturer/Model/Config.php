<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Model_Config
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Model_Config 
    extends Mage_Eav_Model_Config
{
    

    const XML_PATH_LIST_DEFAULT_SORT_BY = 
        'perficient_manufacturer/frontend/default_sort_by';

    
    /**
     * StoreId Property.
     * 
     * @var null
     */
    protected $_storeId = null;


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('perficient_manufacturer/config');
    }//end _construct()


    /**
     * Set store id
     *
     * @param integer $storeId store id
     * 
     * @return Perficient_Manufacturer_Model_Config
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
        return $this;
    }//end setStoreId()


    /**
     * Return store id, if is not set return current app store
     *
     * @return integer
     */
    public function getStoreId()
    {
        if ($this->_storeId === null) {
            return Mage::app()->getStore()->getId();
        }
        return $this->_storeId;
    }//end getStoreId()


    /**
     * Retrieve Attributes Used for Sort by as array
     * key = code, value = name
     *
     * @return array
     */
    public function getAttributeUsedForSortByArray()
    {
        $title = Mage::helper('perficient_manufacturer')->__(
            'Manufacturer Title'
        );

        $updateTime = Mage::helper('perficient_manufacturer')->__(
            'Recent Manufacturer'
        );

        $options = array(
                    'title'       => $title,
                    'update_time' => $updateTime,
                   );

        return $options;
    }//end getAttributeUsedForSortByArray()


    /**
     * Retrieve resource model
     *
     * @return Perficient_Manufacturer_Model_Resource_Eav_Mysql4_Config
     */
    protected function _getResource()
    {
        return Mage::getResourceModel('perficient_manufacturer/config');
    }//end _getResource()


    /**
     * Retrieve Manufacturer List Default Sort By
     *
     * @param mixed $store store
     * 
     * @return string
     */
    public function getManufacturerListDefaultSortBy($store = null)
    {
        return Mage::getStoreConfig(
            self::XML_PATH_LIST_DEFAULT_SORT_BY, $store
        );
    }//end getManufacturerListDefaultSortBy()


}//end class

