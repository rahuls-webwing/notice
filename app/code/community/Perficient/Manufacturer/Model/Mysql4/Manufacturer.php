<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Model_Mysql4_Manufacturer
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Model_Mysql4_Manufacturer 
    extends Mage_Core_Model_Mysql4_Abstract
{

    
    /**
     * _construct method
     * 
     * @return void
     */
    public function _construct()
    {
        // Note that the manufacturer_id refers 
        // to the key field in your database table.
        $this->_init('perficient_manufacturer/manufacturer', 'manufacturer_id');
    }//end _construct()


    /**
     * Process Manufacturer data before saving
     *
     * @param Mage_Core_Model_Abstract $object object data
     * 
     * @return Perficient_Manufacturer_Model_Resource_Manufacturer
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$this->getIsUniqueManufacturerToStores($object)) {
            Mage::throwException(
                Mage::helper('perficient_manufacturer')->__(
                    'A manufacturer URL key for specified store already exists.'
                )
            );
        }

        if ($this->isNumericManufacturerIdentifier($object)) {
            Mage::throwException(
                Mage::helper('perficient_manufacturer')->__(
                    'The Manufacturer URL key cannot consist only of numbers.'
                )
            );
        }
        // modify create / update dates
        if ($object->isObjectNew() && !$object->hasCreationTime()) {
            $object->setCreationTime(
                Mage::getSingleton('core/date')->gmtDate()
            );
        }

        $object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());

        return parent::_beforeSave($object);
    }//end _beforeSave()


    /**
     * Check whether manufacturer identifier is valid
     *
     * @param Mage_Core_Model_Abstract $object object data
     *  
     * @return bool
     */
    protected function isNumericManufacturerIdentifier(
        Mage_Core_Model_Abstract $object
    ) {
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }//end isNumericManufacturerIdentifier()


    /**
     * Check for unique of identifier of manufacturer(s) to selected store(s).
     *
     * @param Mage_Core_Model_Abstract $object object data
     * 
     * @return bool
     */
    public function getIsUniqueManufacturerToStores(
        Mage_Core_Model_Abstract $object
    ) {
        if (Mage::app()->isSingleStoreMode() 
            || !$object->hasData('store_ids')
        ) {
            $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
        } else {
            $stores = (array)$object->getData('store_ids');
        }

        $selectId        = $this->_getCommanManufacturer(
            $object->getData('manufacturer'), $stores
        );
        $fetchedSelectId = $this->_getWriteAdapter()->fetchRow($selectId);

        if (!$object['manufacturer_id']) {
            if ($fetchedSelectId['manufacturer_id']) {
                return false;
            }
        } elseif (
            $object['manufacturer'] == $fetchedSelectId['manufacturer'] 
            && $object['manufacturer_id'] != $fetchedSelectId['manufacturer_id']
        ) {
                return false;
        }

        $select = $this->_getLoadByIdentifierSelect(
            $object->getData('identifier'), $stores
        );

        if ($object->getId()) {
            $select->where('mps.manufacturer_id <> ?', $object->getId());
        }

        if ($this->_getWriteAdapter()->fetchRow($select)) {
            return false;
        }
        return true;
    }//end getIsUniqueManufacturerToStores()


    /**
     * Load store Ids array
     *
     * @param Perficient_Manufacturer_Model_Manufacturer $object object data
     *
     * @return void
     */
    public function loadStoreIds(
        Perficient_Manufacturer_Model_Manufacturer $object
    ) {
        $pollId   = $object->getId();
        $storeIds = array();
        if ($pollId) {
            $storeIds = $this->lookupStoreIds($pollId);
        }
        $object->setStoreIds($storeIds);
    }//end loadStoreIds()


    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id id
     * 
     * @return array
     */
    public function lookupStoreIds($id)
    {
        return $this->_getReadAdapter()->fetchCol(
            $this->_getReadAdapter()->select()
            ->from(
                $this->getTable(
                    'perficient_manufacturer/store'
                ),
                'store_id'
            )
            ->where("{$this->getIdFieldName()} = :id_field"),
            array(':id_field' => $id)
        );
    }//end lookupStoreIds()


    /**
     * Get manufacturer name
     * 
     * @param integer $id      id
     * @param integer $storeId id store id
     * 
     * @return array
     */
    public function getManufacturerName($id, $storeId)
    {
        $stores = array(
                   Mage_Core_Model_App::ADMIN_STORE_ID, 
                   $storeId,
                  );
        $select = $this->_getReadAdapter()->select()
            ->from(
                array('eaov' => $this->getTable('eav/attribute_option_value'))
            )->join(
                array('eao' => $this->getTable('eav/attribute_option')),
                'eaov.option_id = eao.option_id',
                array()
            )
           ->where('eao.option_id = ?', $id)
           ->where('eaov.store_id IN (?)', $stores);

        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('eaov.value');
            return $this->_getReadAdapter()->fetchOne($select);
    }//end getManufacturerName()


    /**
     * Retrieve load select with filter by manufacturer, store and activity
     * 
     * @param integer $id       id
     * @param integer $store    store
     * @param integer $isActive is active flag
     * 
     * @return Varien_Db_Select
     */
    protected function _getCommanManufacturer($id, $store, $isActive = null)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('mp' => $this->getMainTable()))
            ->join(
                array(
                 'mps' => $this->getTable('perficient_manufacturer/store'),
                ),
                'mp.manufacturer_id = mps.manufacturer_id',
                array()
            )
            ->where('mp.manufacturer = ?', $id)
            ->where('mps.store_id IN (?)', $store);

        if (!is_null($isActive)) {
            $select->where('mp.status = ?', $isActive);
        }
        return $select;
    }//end _getCommanManufacturer()


    /**
     * Retrieve load select with filter by identifier, store and activity
     *
     * @param string    $identifier identifier
     * @param int|array $store      store  
     * @param int       $isActive   is active flag
     * 
     * @return Varien_Db_Select
     */
    protected function _getLoadByIdentifierSelect(
        $identifier, $store, $isActive = null
    ) {
        $select = $this->_getReadAdapter()->select()
            ->from(array('mp' => $this->getMainTable()))
            ->join(
                array(
                 'mps' => $this->getTable('perficient_manufacturer/store'),
                ),
                'mp.manufacturer_id = mps.manufacturer_id',
                array()
            )
            ->where('mp.identifier = ?', $identifier)
            ->where('mps.store_id IN (?)', $store);

        if (!is_null($isActive)) {
            $select->where('mp.status = ?', $isActive);
        }
        return $select;
    }//end _getLoadByIdentifierSelect()


    /**
     * Check if manufacturer identifier exist for specific store
     * return page id if page exists
     * 
     * @param integer $identifier identifier
     * @param integer $storeId    storeid
     * 
     * @return object
     */
    public function checkIdentifier($identifier, $storeId)
    {
        $stores = array(
                   Mage_Core_Model_App::ADMIN_STORE_ID, $storeId,
                  );
        $select = $this->_getLoadByIdentifierSelect($identifier, $stores, 1);
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('mp.manufacturer_id')
            ->order('mps.store_id DESC')
            ->limit(1);

        return $this->_getReadAdapter()->fetchOne($select);
    }//end checkIdentifier()


    /**
     * Delete current manufacturer from the table 
     * perficient_manufacturer_store and then
     * insert to update "manufacturer to store" relations
     *
     * @param Mage_Core_Model_Abstract $object object
     *
     * @return void
     */
    public function saveManufacturerStore(Mage_Core_Model_Abstract $object)
    {
        /** stores */
        $deleteWhere = $this->_getReadAdapter()->quoteInto(
            'manufacturer_id = ?', $object->getId()
        );
        $this->_getReadAdapter()->delete(
            $this->getTable('perficient_manufacturer/store'), $deleteWhere
        );

        foreach ($object->getStoreIds() as $storeId) {
            $manufacturerStoreData = array(
                                      'manufacturer_id' => $object->getId(),
                                      'store_id'        => $storeId,
                                     );
            $this->_getWriteAdapter()->insert(
                $this->getTable('perficient_manufacturer/store'), 
                $manufacturerStoreData
            );
            if ($storeId === '0') {
                break;
            }
        }
    }//end saveManufacturerStore()


}//end class
