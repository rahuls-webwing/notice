<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Model_Status
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Model_Status extends Varien_Object
{
    
    
    /**
     * STATUS ENABLED FLAG
     */
    const STATUS_ENABLED = 1;

    /**
     * STATUS DISABLED FLAG
     */
    const STATUS_DISABLED = 0;


    /**
     * Get All Options
     * 
     * @return array
     */
    static public function getAllOptions()
    {
        $enabled  = Mage::helper('perficient_manufacturer')->__('Enabled');
        $disabled = Mage::helper('perficient_manufacturer')->__('Disabled');

        return array(
                self::STATUS_ENABLED  => $enabled,
                self::STATUS_DISABLED => $disabled,
               );
    }//end getAllOptions()


}//end class
