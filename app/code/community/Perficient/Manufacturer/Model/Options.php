<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Model_Options
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Model_Options extends Mage_Core_Model_Abstract
{

    
    /**
     * Get Manufacturers Method
     * 
     * @return array
     */
    public function getManufacturers()
    {
        $attributeCode = Mage::helper(
            'perficient_manufacturer'
        )->getManufacturersAttributeCode();

        return $this->__getAttributeData(
            Mage_Catalog_Model_Product::ENTITY, $attributeCode
        );
    }//end getManufacturers()


    /**
     * Get Attribute Data Method
     *
     * @param string $entity entity
     * @param string $code   code
     *
     * @return object
     */
    private function __getAttributeData($entity, $code)
    {
        $options = $data = array();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute($entity, $code);

        if ($attribute->usesSource()) {
            $data = $attribute->getSource()->getAllOptions(false);
        }

        if (!empty($data)) {
            foreach ($data as $item) {
                $options[$item['value']] = $item['label'];
            }
        }
        return $options;
    }//end __getAttributeData()


}//end class
