<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Model_Manufacturer
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Model_Manufacturer 
    extends Mage_Core_Model_Abstract
{


    /**
     * _construct method
     * 
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('perficient_manufacturer/manufacturer');
    }//end _construct()


    /**
     * Check if manufacturer identifier exist for specific store
     * return manufacturer id if manufacturer exists
     *
     * @param string $identifier identifier
     * @param int    $storeId    store id
     * 
     * @return int
     */
    public function checkIdentifier($identifier, $storeId)
    {
        return $this->_getResource()->checkIdentifier($identifier, $storeId);
    }//end checkIdentifier()


    /**
     * Save manufacturer store, after manufacturer save
     *
     * @return Perficient_Manufacturer_Model_Manufacturer
     */
    protected function _afterSave()
    {
        if ($this->hasStoreIds()) {
            $this->_getResource()->saveManufacturerStore($this);
        }
        return parent::_afterSave();
    }//end _afterSave()

    
    /**
     * Add Store Id
     *
     * @param integer $storeId store id
     * 
     * @return Perficient_Manufacturer_Model_Manufacturer
     */
    public function addStoreId($storeId)
    {
        $ids = $this->getStoreIds();
        if (!in_array($storeId, $ids)) {
            $ids[] = $storeId;
        }
        $this->setStoreIds($ids);
        return $this;
    }//end addStoreId()


    /**
     * Get Store Ids
     * 
     * @return array
     */
    public function getStoreIds()
    {
        $ids = $this->_getData('store_ids');
        if (is_null($ids)) {
            $this->loadStoreIds();
            $ids = $this->getData('store_ids');
        }
        return $ids;
    }//end getStoreIds()


    /**
     * Load Store Ids
     * 
     * @return void
     */
    public function loadStoreIds()
    {
        $this->_getResource()->loadStoreIds($this);
    }//end loadStoreIds()

    
    /**
     * Add Error
     * 
     * @param string $error error message
     *
     * @return void
     */
    function addError($error)
    {
        $this->_errors[] = $error;
    }//end addError()


    /**
     * Get Error
     *
     * @return void
     */
    function getErrors()
    {
        return $this->_errors;
    }//end getErrors()


    /**
     * Reset Errors
     * 
     * @return void
     */
    function resetErrors()
    {
        $this->_errors = array();
    }//end resetErrors()


    /**
     * Print Error method
     * 
     * @param string  $error error message
     * @param integer $line  line number
     * 
     * @return void
     */
    function printError($error, $line = null)
    {
        if ($error == null) return false;
        $img     = 'error_msg_icon.gif';
        $liStyle = 'background-color:#FDD; ';
        echo '<li style="'.$liStyle.'">';
        echo '<img src="'.
            Mage::getDesign()->getSkinUrl('images/'.$img).
        '" class="v-middle"/>';
        echo $error;
        if ($line) {
            echo '<small>, Line: <b>'.$line.'</b></small>';
        }
        echo "</li>";
    }//end printError()

    
    /**
     * Get Manufacturer Name Method
     * 
     * @param integer $id      id of manufacturer
     * @param integer $storeId store id
     * 
     * @return string
     */
    public function getManufacturerName($id, $storeId)
    {
        return $this->_getResource()->getManufacturerName($id, $storeId);
    }//end getManufacturerName()


}//end class
