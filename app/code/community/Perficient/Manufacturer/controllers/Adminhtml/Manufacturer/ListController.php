<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-community.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUNITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   GIT:1.0.8
 * @link      [No Url]
 */



/**
 * Perficient_Manufacturer_Adminhtml_Manufacturer_ListController
 *
 * @category  Perficient
 * @package   Perficient_Manufacturer
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   OSL http://shop.perficient.com/license-community.txt
 * @version   Release:1.0.8
 * @link      [No Url]
 */
class Perficient_Manufacturer_Adminhtml_Manufacturer_ListController 
    extends Mage_Adminhtml_Controller_Action
{

    
    /**
     * Index Action
     * 
     * @return void
     */
    public function indexAction()
    {
        $this->_title(
            $this->__('Perficient Extensions')
        )->_title($this->__('Manufacturer'));
        $this->loadLayout();
        $this->_setActiveMenu('zextensions/perficient_manufacturer');
        $this->renderLayout();
    }//end indexAction()

    
    /**
     * New Action
     * 
     * @return void
     */
    public function newAction()
    {
        $this->_forward('edit');
    }//end newAction()


    /**
     * Edit action
     * 
     * @return void
     */
    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = $this->_initManufacturer('id');
        $model = Mage::getModel(
            'perficient_manufacturer/manufacturer'
        )->load($id);

        if (!$model->getId() && $id) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('perficient_manufacturer')->__(
                    'This manufacturer no longer exists.'
                )
            );
            $this->_redirect('*/*/');
            return;
        }
        
        $title = $this->__('Add Manufacturer');
        $this->_title($model->getId() ? $model->getTitle() : $title);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->loadLayout();
        $this->_setActiveMenu('zextensions/perficient_manufacturer');

        $eTitle = Mage::helper('perficient_manufacturer')->__(
            'Edit Manufacturer'
        );
        $aTitle = Mage::helper('perficient_manufacturer')->__(
            'Add Manufacturer'
        );
        
        $cond = ($id) ? $eTitle : $aTitle;

        $this->_addBreadcrumb($cond,$cond)->renderLayout();
    }//end editAction()


    /**
     * Save action
     *
     * @return void
     */
    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {
            $id    = $this->getRequest()->getParam('id');
            $model = $this->_initManufacturer();

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('perficient_manufacturer')
                        ->__('This manufacturer no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }

            $manufacturerName = $model->getManufacturerName(
                $data['manufacturer'], 
                Mage::app()->getStore()->getId()
            );

            if ((bool) $data['identifier']) {
                $identifier = $data['identifier'];
            } else {
                $identifier = $manufacturerName;
            }

            $data['identifier'] = Mage::getModel('perficient_manufacturer/url')
                ->formatUrlKey($identifier);
            
            // save manufacturer logo and banner
            $fieldName = array(
                          'manufacturer_logo',
                          'manufacturer_banner',
                         );

            foreach ($fieldName as $field) {                
                if (!empty($_FILES[$field]['name']) ) {
                    try {
                        $uploader = new Varien_File_Uploader($field);
                        $uploader->setAllowedExtensions(
                            array(
                             'jpg','jpeg','gif','png',
                            )
                        );
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $path = 
                            Mage::getBaseDir('media').DS.'manufacturer' . DS;

                        $_FILES[$field]['name'] = 
                        str_replace(
                            ' ', '-', $_FILES[$field]['name']
                        );

                        $result = $uploader->save(
                            $path,
                            $_FILES[$field]['name']
                        );
                        if (!empty($data[$field]['value'])) {
                            //can also delete file from fs
                            unlink($path.$data[$field]['value']);
                        }
                        $model->setData(
                            array($field => $uploader->getUploadedFileName())
                        );
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addSuccess(
                            $e->getMessage()
                        );
                    }
                } else {
                    unset($data[$field]);
                }
            }
            // save model
            try {
                if (!empty($data)) {
                    $model->addData($data);
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('perficient_manufacturer')->__(
                        'The manufacturer has been saved.'
                    )
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('perficient_manufacturer')->__(
                        'Unable to save the manufacturer.'
                    )
                );
                $redirectBack = true;
                Mage::logException($e);
            }
            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/');
    }//end saveAction()


    /**
     * Delete action
     *
     * @return void
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('perficient_manufacturer/manufacturer');
                $model->load($id);
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('perficient_manufacturer')
                        ->__('The manufacturer has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('perficient_manufacturer')->__(
                        'An error occurred while deleting manufacturer data. 
                        Please review log and try again.'
                    )
                );
                Mage::logException($e);
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('perficient_manufacturer')
                ->__('Unable to find a manufacturer to delete.')
        );
        // go to grid
        $this->_redirect('*/*/');
    }//end deleteAction()


    /**
     * Delete specified manufacturer using grid massaction
     *
     * @return true
     */
    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('manufacturer');
        if (!is_array($ids)) {
            $this->_getSession()->addError(
                $this->__('Please select manufacturer(s).')
            );
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton(
                        'perficient_manufacturer/manufacturer'
                    )->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__(
                        'Total of %d record(s) have been deleted.', 
                        count($ids)
                    )
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('perficient_manufacturer')->__(
                        'An error occurred while mass deleting manufacturer. 
                        Please review log and try again.'
                    )
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }//end massDeleteAction()


    /**
     * Update specified manufacturer status using grid massaction
     *
     * @return void
     */
    public function massStatusAction()
    {
        $ids = $this->getRequest()->getParam('manufacturer');
        if (!is_array($ids)) {
            $this->_getSession()->addError(
                $this->__('Please select manufacturer(s).')
            );
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton(
                        'perficient_manufacturer/manufacturer'
                    )->load($id);
                    $model->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__(
                        'Total of %d record(s) have been updated', 
                        count($ids)
                    )
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('perficient_manufacturer')->__(
                        'An error occurred while mass updating manufacturer. 
                        Please review log and try again.'
                    )
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }//end massStatusAction()


    /**
     * Load Manufacturer from request
     *
     * @param string $idFieldName Id Field Name 
     * 
     * @return Perficient_Manufacturer_Model_Manufacturer $model
     */
    protected function _initManufacturer($idFieldName = 'manufacturer_id')
    {
        $id    = (int)$this->getRequest()->getParam($idFieldName);
        $model = Mage::getModel('perficient_manufacturer/manufacturer');
        if ($id) {
            $model->load($id);
        }
        if (!Mage::registry('current_manufacturer')) {
            Mage::register('current_manufacturer', $model);
        }
        return $model;
    }//end _initManufacturer()


    /**
     * Render Manufacturer grid
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }//end gridAction()


}//end class
